package com.example.demo;

public class ThreadTest implements Runnable{

    @Override
    public void run() {
        for(int i = 0; i < 100; i++){
            System.out.println(i);
        }

    }

    public static void main(String[] args) {
     /*   ThreadTest threadTest = new ThreadTest();

        threadTest.run();*/

        Thread thread = new Thread(){
            @Override
            public void run() {
                ping();
            }
        };

        thread.run();
        System.out.println("pong");

    }

    static void ping() {
        System.out.println("ping");
    }
}
